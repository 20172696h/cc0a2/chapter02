package pe.uni.rsaenzc.checkbox;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.CheckBox;
import android.widget.TextView;

public class CheckBoxActivity extends AppCompatActivity {

    TextView textView;
    CheckBox checkBoxFemale;
    CheckBox checkBoxMale;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_box);

        textView = findViewById(R.id.text_view);
        checkBoxFemale = findViewById(R.id.check_box_female);
        checkBoxMale = findViewById(R.id.check_box_male);

        checkBoxFemale.setOnClickListener(v -> {
            if(checkBoxFemale.isChecked()){
                textView.setText(R.string.text_female);
            }else{
                textView.setText(R.string.msg);
            }
        });

        checkBoxFemale.setOnClickListener(v -> {
            if(checkBoxMale.isChecked()){
                textView.setText(R.string.text_male);
            }else{
                textView.setText(R.string.msg);
            }
        });
    }
}