package pe.uni.rsaenzc.messages;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;

public class MessagesActivity extends AppCompatActivity {

    Button buttonToast, buttonSnackBar, buttonDialog;
    LinearLayout linearLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_messages);

        buttonToast = findViewById(R.id.button_message_1);
        buttonSnackBar = findViewById(R.id.button_message_2);
        buttonDialog = findViewById(R.id.button_message_3);

        buttonToast.setOnClickListener(v -> Toast.makeText(getApplicationContext(),R.string.msg_toast,Toast.LENGTH_LONG));

        buttonSnackBar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Snackbar.make(linearLayout, R.string.msg_snack_bar, Snackbar.LENGTH_INDEFINITE).setAction(R.string.button_snack_bar_text,new View.OnClickListener(){
                    @Override
                    public void onClick(View v){

                    }
                }).show();
            }
        });

        buttonToast.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(),R.string.msg_toast,Toast.LENGTH_LONG);
            }
        });

        buttonDialog.setOnClickListener(v -> {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
                alertDialog
                        .setTitle(R.string.dialog_tittle)
                        .setMessage(R.string.dialog_text)
                        .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        })
                        .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                // Luego
                            }
                        }).show();
                alertDialog.create();

        });
    }
}