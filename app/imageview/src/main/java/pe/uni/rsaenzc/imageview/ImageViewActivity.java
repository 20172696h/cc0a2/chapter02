package pe.uni.rsaenzc.imageview;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageView;

public class ImageViewActivity extends AppCompatActivity {

    ImageView imageview;
    Button button;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_view);

        imageview = findViewById(R.id.image_view);
        button = findViewById(R.id.button);

        button.setOnClickListener(v -> imageview.setImageResource(R.drawable.b));


    }
}