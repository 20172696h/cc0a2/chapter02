package pe.uni.rsaenzc.intents;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class SecondActivity extends AppCompatActivity {

    TextView textViewText, textViewNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        textViewText = findViewById(R.id.text_view_number);
        textViewNumber = findViewById(R.id.text_view_number);

        Intent intent2 = getIntent();
        String text = intent2.getStringExtra("TEXT");
        int number = intent2.getIntExtra("NUMBER",0);

        textViewText.setText(text);
        textViewNumber.setText(String.valueOf(number));

    }

}
